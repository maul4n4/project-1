<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = "brand";
    protected $fillable = ["merk"];

    public function gadget(){
        return $this->hasMany('App\Gadget');
    }

}
