<?php

namespace App;
use DB;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profil";
    protected $fillable = ["bio", "avatar", "user_id"];

    public function gadget(){
        return $this->hasOne('App\User');
    }
}
