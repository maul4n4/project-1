<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function login(){
        return view('user.login');
    }

    public function register(){
        return view('user.register');
    }

    public function profile(){
        return view('user.profile');
    }

    public function kirim(Request $request){
        $nama = $request['nama'];
        $bio = $request['bio'];
        $avatar = $request['avatar'];
        return view('user.profile', compact('nama', 'bio', 'avatar'));
    }

}
