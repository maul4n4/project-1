<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Profile;
use DB;

class ProfileController extends Controller
{
    public function index(){
        $profile = Profile::where('user_id', Auth::id())->First();

        return view('profile.index', compact('profile'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'bio' => 'required',
            'avatar' => 'required'
        ]);

        $profile = Profile::find($id);

        $profile->bio = $request['bio'];
        $profile->avatar = $request['avatar'];

        $profile->save();

        return redirect('/profile');
    }
}
