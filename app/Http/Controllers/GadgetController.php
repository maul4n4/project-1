<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Gadget;
use File;
class GadgetController extends Controller
{
    public function index()
    {
        $gadget= Gadget::all();
        return view('gadget.index',compact('gadget'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brand = DB::table('brand')->get();
        return view('gadget.create',compact('brand'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'nama'=>'required',
            'gambar'=>'required |image|mimes:jpeg,jpg,png|max:2048',

            'layar'=>'required',
            'processor'=>'required',
            'internal'=>'required',
            'ram'=>'required',
            'camera'=>'required',
            'sistem_operasi'=>'required',
            'brand_id'=>'required',
        ]);
        
        $gambar = time().'.'.$request->gambar->extension();
        $request->gambar->move(public_path('images'),$gambar);

        $gadget=new Gadget;
        $gadget->nama = $request->nama;
        $gadget->gambar = $gambar;

        $gadget->layar = $request->layar; 
        $gadget->processor = $request->processor; 
        $gadget->internal = $request->internal; 
        $gadget->ram = $request->ram; 
        $gadget->camera = $request->camera; 
        $gadget->sistem_operasi = $request->sistem_operasi; 
       
        $gadget->brand_id = $request->brand_id;

        $gadget->save();
        return redirect('/gadget');

    }

    public function edit($id)
    {
       $brand = DB::table('brand')->get();
       $gadget = Gadget::findOrFail($id);
       return view('gadget.edit',compact('gadget','brand'));
    }

    public function show($id)
    {
        
        $gadget = Gadget::findOrfail($id);
        return view('gadget.show',compact('gadget'));
    }
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'nama' => 'required',
            'gambar'=>'required |image|mimes:jpeg,jpg,png|max:2048',

            'layar'=>'required',
            'processor'=>'required',
            'internal'=>'required',
            'ram'=>'required',
            'camera'=>'required',
            'sistem_operasi'=>'required',
            'brand_id'=>'required',
            
        ]);
 
        $gadget = Gadget::find($id);

        if($request->has('gambar')){
            $gambar = time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('images'),$gambar);
    
            $gadget->nama = $request->nama;
            $gadget->gambar = $gambar;
    
            $gadget->layar = $request->layar; 
            $gadget->processor = $request->processor; 
            $gadget->internal = $request->internal; 
            $gadget->ram = $request->ram; 
            $gadget->camera = $request->camera; 
            $gadget->sistem_operasi = $request->sistem_operasi; 
           
            $gadget->brand_id = $request->brand_id;
            
    
        }else{
            $gadget->nama= $request->nama;
    
            $gadget->layar = $request->layar; 
            $gadget->processor = $request->processor; 
            $gadget->internal = $request->internal; 
            $gadget->ram = $request->ram; 
            $gadget->camera = $request->camera; 
            $gadget->sistem_operasi = $request->sistem_operasi; 
            $gadget->brand_id = $request->brand_id;
        }

        
        $gadget->update();

        return redirect('/gadget');
    }

    public function destroy($id)
    {
        $gadget = Gadget::find($id);
        $gadget->delete();
        
        return redirect('/gadget');
    }
    

  
}
