<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use File;
use DB;

use RealRashid\SweetAlert\Facades\Alert;
class BrandController extends Controller
{
    public function create()
    {
        $brand=DB::table('brand')->get();
        return view('brand.create', compact('brand'));
    }

    public function store(Request $request)
    {
        $brand = new Brand;

        $brand->merk = $request->merk;

        $brand->save();
        
        Alert::success('SUKSES', 'Tambah Brand Berhasil');
        return redirect('/brand');
        
    }

    public function index()
    {
        $brand = Brand::all();
        return view('brand.index',compact('brand'));
    }

    public function show($id)
    {
        $brand = Brand::find($id);
        return view('brand.show',compact('brand'));
    }
    public function edit($id)
    {
       $brand = Brand::findOrFail($id);
       return view('brand.edit',compact('brand'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'merk' => 'required',
            
        ]);
 
        $brand = Brand::find($id);
        $brand->merk = $request->merk;
    
        $brand->update();
        
        Alert::success('SUKSES', 'Data Berhasil Dirubah');
        return redirect('/brand');
    }

    public function destroy($id)
    {
        $brand = Brand::find($id);
        $brand->delete();
        Alert::success('SUKSES', 'Data Berhasil Dihapus');
        return redirect('/brand');
    }
    
}
