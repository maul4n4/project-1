<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Gadget extends Model
{
    protected $table = 'gadget';
    protected $fillable = ['nama',"processor","internal","ram","camera","sistem_informasi","brand_id"];

    public function brand(){
        return $this->belongsto('App\Brand');
    }
}
