@extends('layout.template')

@section('Judul')
  User Profile  
@endsection

@section('content')
<a href="/profile" class="btn btn-primary">Input Cast</a>
    
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Bio</th>
        <th scope="col">Avatar</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($castview as $key => $item)
          <tr>
              <td>{{$key + 1}}</td>
              <td>{{$item->nama}}</td>
              <td>{{$item->bio}}</td>
              <td>{{$item->avatar}}</td>
              <td>
                
                <form action="/profile/{{$item->id}}" method="POST">
                    @method('delete')
                    @csrf
                    <a href="/cast/{{$item->id}}" class="btn btn-warning btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
                </td>
          </tr>
      @empty
      <tr>
          <td>Data Belum Terisi</td>
      </tr> 
      @endforelse
  </table>

  @endsection