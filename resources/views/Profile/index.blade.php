@extends('layout.master')

@section('judul')
	Halaman Update Profile
@endsection

@section('content')
 
<form action="/profile/{{$profile->id}}" method="POST">
    @method('PUT')
    @csrf
		<div class="form-group">
		<label>bio</label>
		<input type="number" class="form-control" value="{{$profile->bio}}" name="bio" placeholder="bio">
		@error('bio')
		<div class="alert alert-danger">
			{{ $message }}
		</div>
    	@enderror
    </div>
    
	<div class="form-group">
        <label>Avatar</label>
        <input type="text" class="form-control" value="{{$profile->avatar}}" name="avatar" placeholder="avatar">
         @error('avatar')
    	  <div class="alert alert-danger">
             {{ $message }}
          </div>
        @enderror
    </div>
        <button type="submit" class="btn btn-primary">Update</button>
</form>  



@endsection