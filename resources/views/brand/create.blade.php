
@extends('layout.template')

@section('judul')
<h1>Buat Data Brand Baru!</h1> 
@endsection

@section('content')
<form action="/brand" method="POST">
    @csrf
    <div class="form-group">
      <label >Nama Brand</label>
      <input type="text" name="merk" class="form-control" >
    </div>
        @error('merk')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection