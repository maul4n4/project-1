
@extends('layout.template')

@section('judul')
<h1>Edit Data Brand</h1> 
@endsection

@section('content')
<form action="/brand/{{$brand->id}}" method="POST">
    @csrf
    @method("put")
    <div class="form-group">
      <label >Nama Brand</label>
      <input type="text" name="merk" class="form-control" value="{{$brand->merk}}">

    </div>
        @error('merk')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection