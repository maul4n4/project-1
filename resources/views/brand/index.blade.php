@extends('layout.template')

@section('judul')
Halaman List Brand
@endsection

@section('judul')
Halaman List Brand
@endsection

@section('content') 


<div class="ml-2 mt-3" style="width=100px">
  <a class="btn btn-success mb-3 mk-3 " href="/brand/create" > Tambah Data</a>
</div>


<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>  
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
     @forelse ($brand as $key=>$item)
         <tr>
             <td>{{$key+1}}</td>
             <td>{{$item->merk}}</td>
             <td>
                
                <form action="/brand/{{$item->id}}" method="POST">
                    @method('delete')
                    @csrf
                    <a class="btn btn-info btn-sm" href="/brand/{{$item->id}}" >Detail</a>
               
                    <a class="btn btn-warning btn-sm" href="/brand/{{$item->id}}/edit" role="button">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                  
                </form>
                
             </td>
         </tr>
     @empty
         <tr>
             <td>Data masih kosong </td>
         </tr>
     @endforelse
    </tbody>
  </table>

@endsection