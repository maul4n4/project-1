@extends('layout.template')

@section('judul')
Spesifikasi {{$gadget->brand->merk}} {{$gadget->nama}}
@endsection


@section('content') 
<div class="row">

    <div class="col-4">
        <div class="card" style="width: 250px;">
            
            <img src="{{asset('images/'.$gadget->gambar)}}" class="card-img-top" alt="...">
                <div class="card-body">
                 
                </div>
        </div>
    </div>  

    <div class="col-5">
        <div class="card" style="width: 300px;">
            
                <div class="card-body">

                    <ul class="list-group">
                        <li class="list-group-item list-group-item-info">Layar : {{$gadget->layar }}</li>
                        <li class="list-group-item list-group-item-info">Processor : {{$gadget->processor }}</li>
                        <li class="list-group-item list-group-item-info">Memory Internal : {{$gadget->internal }}</li>
                        <li class="list-group-item list-group-item-info">Memory RAM : {{$gadget->ram }}</li>
                        <li class="list-group-item list-group-item-info">Camera : {{$gadget->camera }}</li>
                        <li class="list-group-item list-group-item-info">Sistem Operasi : {{$gadget->sistem_operasi }}</li>
                       
                    </ul>
                    
                  
                    
                </div>
        </div>
    </div>  

</div>

@endsection
