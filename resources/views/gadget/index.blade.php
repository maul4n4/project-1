@extends('layout.template')

@section('judul')
<h1>List Data Gadget !</h1> 
@endsection

@section('content')

<div class="ml-2 mt-3" style="width=100px">
    <a class="btn btn-success mb-3 mk-3 " href="/gadget/create" > Tambah Data</a>
  </div>

<div class="row">
    @forelse ($gadget as $item)

    <div class="col-3">
        <div class="card" style="width: 200px;">
            
            <div class="card-body">
                <h5 class="card-title mx-3 mb-2">{{$item->nama}}</h5><br>
                <img src="{{asset('images/'.$item->gambar)}}" class="card-img-top" alt="...">
                <form action="/gadget/{{$item->id}}" method="POST" class="mt-2">
                    @method('delete')
                    @csrf
                    <a class="btn btn-info btn-sm" href="/gadget/{{$item->id}}" >Detail</a>
                    <a class="btn btn-warning btn-sm" href="/gadget/{{$item->id}}/edit" role="button">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                  
                </form>
            </div>
        </div>
    </div>  
@empty
    <h4>Data Film belum ada</h4>
@endforelse
</div>



@endsection