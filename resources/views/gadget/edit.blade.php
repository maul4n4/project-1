
@extends('layout.template')

@section('judul')
<h1>Edit Data Gadget!</h1> 
@endsection

@section('content')
<form action="/gadget/{{$gadget->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method("put")

    <div class="form-group">
        <label>Brand</label>
        <select name="brand_id" class="form-control" id="" >
            <option value="">---Pilih Brand---</option>
            @foreach ($brand as $item)
                <option value="{{$item->id}}">{{$item->merk}}</option>
            @endforeach
        </select>
    </div>
    @error('brand_id')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label >Nama Gadget</label>
      <input type="text" name="nama" class="form-control" value="{{$gadget->nama}}">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label >Gambar</label>
        <input type="file" name="gambar" class="form-control" value="{{$gadget->gambar}}" >
    </div>
    @error('gambar')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <div class="form-group">
      <label >Layar</label>
      <input type="text" name="layar" class="form-control" value="{{$gadget->layar}}">
    </div>
    @error('layar')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
 
    <div class="form-group">
        <label >Processor</label>
        <input type="text" name="processor" class="form-control" value="{{$gadget->processor}}">
      </div>
      @error('processor')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label >Internal</label>
        <input type="text" name="internal" class="form-control" value="{{$gadget->internal}}">
      </div>
      @error('internal')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    
      <div class="form-group">
        <label >Ram</label>
        <input type="text" name="ram" class="form-control" value="{{$gadget->ram}}">
      </div>
      @error('ram')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label >Camera</label>
        <input type="text" name="camera" class="form-control" value="{{$gadget->camera}}">
      </div>
      @error('camera')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label >Sistem Operasi</label>
        <input type="text" name="sistem_operasi" class="form-control" value="{{$gadget->sistem_operasi}}">
      </div>
      @error('sistem_operasi')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection