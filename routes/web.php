<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//User
Route::get('/login', 'UserController@login');
Route::post('/profile', 'UserController@kirim');

//Profil
Route::resource('profile', 'ProfileController')->only([
    'index', 'update'
]);

//CRUD Brand
Route::get('/brand/create','BrandController@create');
Route::post('/brand', 'BrandController@store');
Route::get('/brand', 'BrandController@index');
Route::get('/brand/{brand_id}', 'BrandController@show');
Route::get('/brand/{brand_id}/edit', 'BrandController@edit');
Route::put('/brand/{brand_id}', 'BrandController@update');
Route::delete('/brand/{brand_id}', 'BrandController@destroy');

//CRUD Gadget
Route::get('/gadget/create','GadgetController@create');
Route::post('/gadget', 'GadgetController@store');
Route::get('/gadget', 'GadgetController@index');
Route::get('/gadget/{gadget_id}', 'GadgetController@show');
Route::get('/gadget/{gadget_id}/edit', 'GadgetController@edit');
Route::put('/gadget/{gadget_id}', 'GadgetController@update');
Route::delete('/gadget/{gadget_id}', 'GadgetController@destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
