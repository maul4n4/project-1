<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGadgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gadget', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama',45);
            $table->string('gambar',45);
            $table->string('layar',45);
            $table->string('processor',45);
            $table->string('internal',45);
            $table->string('ram',45);
            $table->string('camera',45);
            $table->string('sistem_operasi',45);
            $table->unsignedBigInteger('brand_id');
            $table->foreign('brand_id')->references('id')->on('brand');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gadget');
    }
}
